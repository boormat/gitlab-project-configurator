=============================
Configuration Files Reference
=============================

This is a YAML (or JSON) file which describe all the project settings you want
GPC to handle.

Your configuration files are stored in your in your private "config" project
(the one that are going to define the settings to be applied to your project).
Even if you can use gpc on your own PC to apply the change using your
credentials, it is highly recommended to use the Gitlab Pipeline to perform any
change.

.. note:: Note that configuration can be split into several files
  using the ``include`` keyword, and can be written in various formats:
  YAML, JSON or TOML.

  Include files can even be nested. File path are always relative to the
  current file:

  Example:

  .. code-block:: yaml

    # Top-level include
    include:
      - subfolder/include.yml

  And in ``subfolder/include.yml``, it include another file
  ``subfolder/subsubfolder/anotherinclude.yml`` by giving a path relative to the
  ``subfolder``:

  .. code-block:: yaml

    # subfolder/include.yml include other files by relative path
    include:
      - subsubfolder/anotherinclude.yml

  They simply needs to follow the same schema.

Configuration is actually divided into several sections:

- "**rules**" or "policies": a set of rules are defined and a name is given.
  You can define several "policies", even use inheritance.
- **projects configuration**: for a project or a list of projects, a policy is
  applied, with potentially some exceptions (using "`custom_rules`")
- **groups configuration**: for a group or a list of groups, a policy is
  applied, with potentially some exceptions (using "`custom_rules`")

A "rule" or "policy" is a set of settings to be applied to the GitLab projects.
Each rule should have a unique name.

"Project Configurations" are set of projects that should be follow the same
rules. Same for "Group Configurations"

.. image:: images/rules-projects-config.png

.. note:: If a setting is not defined in GPC, or is set to ``null``, it will be
  **not** changed.

The main purpose of these named rules is to easily configure a set of GitLab
projects, and to easily propagate changes if needed.

A rule can inherit from another one (but multiple inheritance is not supported).

.. image:: ../../data/gpc-usecase.png

Self-documented example
=======================

Here is a example of a single-file config with a set of members profiles,
variables profiles, project rules and config, and group rules and config.

.. note:: Please keep in mind this example may be a little bit out-of-date.

.. code-block:: yaml

    # Define some list of users
    member_profiles:
      - name: mytest_master_approvers
        role: maintainers
        members:
          - calamity.desperados
          - path/to/your/group

    # Define some logically linked set of variables that can be later 'imported'
    # into different profiles.
    variable_profiles:
      SET_OF_VARIABLES:
        - name: SOME_VARIABLE_NAME
          value: some variable value
          protected: true
        # "SOMETHING_PRIVATE" should be defined in your config project!
        - name: SOMETHING_PRIVATE
          value_from_envvar: SOMETHING_PRIVATE  # read from environment variable
          protected: true
          masked: true
      ANOTHER_GROUP_OF_VARIABLES:
        - name: OTHER_VARIABLE
          value: other value

    # Definition of project policies:
    projects_rules:
      ########################################
      # define a default_policy for projects #
      ########################################
      - rule_name: default_policy

        default_branch: master

        description: my description

        ci_config_path: my_ci_config_file.yml

        ci_git_shallow_clone: 50

        auto_cancel_pending_pipelines: "enabled"


        issues_template: Describe the issue.

        merge_commit_template: Merge %{source_branch} into %{target_branch}

        squash_commit_template: |
            %{title}

            %{url}
            %{co_authored_by}

        project_members:
          profiles:
            - mytest_master_approvers
          members:
            - role: developers
              name: calamity.janes
            - role: developers
              name: billy.thekid

        approvers:
          profiles:
            - mytest_master_approvers
          members:
            - roger.duvel
          minimum: 1
          options:
            can_override_approvals_per_merge_request: true
            remove_all_approvals_when_new_commits_are_pushed: false
            enable_self_approval: true
            enable_committers_approvers: true
            selective_code_owner_removals: false

        schedulers:
          - name: schedule_toto
            branch: master
            cron: "0 4 1 * *"
            enabled: true
            tz: UTC # by default
            variables:
              - import: SET_OF_VARIABLES
              - name: OTHER_VARIABLE
                value: other value
              - name: var_2
                value_from_envvar: ENVVAR_VALUE

        protected_branches:
          - pattern: master
            allowed_to_merge:
                role: maintainers
                profiles:
                    - mytest_master_approvers
                members:
                    - robert.tripel
            allowed_to_push: none
            allow_force_push: false # optional
            code_owner_approval_required: true # optional

        protected_tags:
          - pattern: "*"
            allowed_to_create: maintainers

        permissions:
          visibility: internal
          request_access_enabled: true
          wiki_enabled: false
          lfs_enabled: false
          issues_enabled: false
          snippets_enabled: false
          container_registry_enabled: false
          jobs_enabled: false
          merge_requests: false
          packages_enabled: false
          releases_access_level: disabled
          infrastructure_access_level: disabled
          feature_flags_access_level: disabled
          environments_access_level: disabled
          monitor_access_level: disabled
          pages_access_level: disabled
          analytics_access_level: disabled
          forking_access_level: disabled
          security_and_compliance_access_level: disabled

        mergerequests:
          only_allow_merge_if_all_discussions_are_resolved: true
          only_allow_merge_if_pipeline_succeeds: true
          remove_source_branch_after_merge: true
          merge_method: ff
          printing_merge_request_link_enabled: true
          default_template: Describe briefly what this MR is for and what it does.

        artifacts:
          keep_latest_artifact: false

        labels:
          - name: label_name
            color: '#FF0000'

        runners:
          - runner_id: 666
            enabled: true

        variables:
          - name: MY_VARIABLE
            value: My variable value
            variable_type: env_var
          - name: MY_VARIABLE_MASKED
            value: masked_variable
            variable_type: env_var
            masked: true

        integrations:
          jira:
            url: 'https://my.jira.server'
            jira_issue_transition_id: 101
            username: my bot
            password_from_envvar: ENVVAR_PASSWORD
            trigger_on_commit: false

        push_rules:
          remove: false
          dont_allow_users_to_remove_tags: true
          member_check: true
          prevent_secrets: true
          commit_message: '^(.+)$'
          commit_message_negative: 'truc'
          branch_name_regex: '^(.+)$'
          author_mail_regex: '^(.+)$'
          prohibited_file_name_regex: '^(.+)$'
          max_file_size: 500
          reject_unsigned_commits: false

        deploy_keys:
          - id: 123456 # ID of your deploy key
            can_push: true # whether or not the deploy key is allowed to push

        badges:
          # Pipeline Status badge
          - name: pipeline
            link_url: "%{gpc_gitlab_url}/%{project_path}/commits/%{default_branch}"
            image_url: "%{gpc_gitlab_url}/%{project_path}/badges/%{default_branch}/pipeline.svg"
          - name: coverage
            link_url: "http://my.artifactory.server/artifactory/reports/coverage/%{project_path}/%{default_branch}/"
            image_url: "%{gpc_gitlab_url}/%{project_path}/badges/%{default_branch}/coverage.svg"

      ######################################################
      # define another_policy for other type of projects   #
      ######################################################
      - rule_name: another_policy
        inherits_from: default_policy

        permissions:
          visibility: private

        mergerequests:
          only_allow_merge_if_all_discussions_are_resolved: true
          only_allow_merge_if_pipeline_succeeds: false
          merge_method: merge

        variables:
          - name: MY_VARIABLE_FROM_ENVVAR
            value_from_envvar: ENVIRONMENT_VARIABLE_NAME
            protected: true
            variable_type: file

    # Apply the project rules on some projects
    projects_configuration:
      - paths:
          - groupname/my_project01
          - groupname/my_project02
        rule_name: default_policy

      - paths:
          - another_group
        recursive: true
        excludes:
          - another_group/excluded/project
          - ^another_group\/to\/exclude\/with\/a\/.*regex$
        rule_name: default_policy

      # For some project you can add customizations
      - paths:
          - groupname/my_project03
        not_seen_yet_only: true  # only apply if not already applied previously
        rule_name: default_policy
        custom_rules:
          protected_tags: null  # ignore change on protected tags
          mergerequests:
            merge_method: rebase_merge  # force rebase merge
            only_allow_merge_if_pipeline_succeeds: false

    # For group rules only variables and _members are available.
    groups_rules:
      ########################################
      # define a default_policy for groups   #
      ########################################
      - rule_name: default_policy
        description: my description
        variables:
          - name: MY_VARIABLE
            value: My variable value
            variable_type: env_var
          - name: MY_VARIABLE_MASKED
            value: masked_variable
            variable_type: env_var
            masked: true
        group_members:
          profiles:
            - mytest_master_approvers
          members:
            - role: developers
              name: calamity.janes
            - role: developers
              name: billy.thekid

      ######################################################
      # define another_policy for other type of groups     #
      ######################################################
      - rule_name: another_policy
        inherits_from: default_policy
        variables:
          - name: MY_VARIABLE_FROM_ENVVAR
            value_from_envvar: ENVIRONMENT_VARIABLE_NAME
            protected: true
            variable_type: file

    groups_configuration:
      # It is possible to configure a list of groups
      - paths:
        - groupname/my_group01
        - groupname/my_group02
        rule_name: default_policy

Note that in order to configure a GitLab project, using a named rule is **mandatory**!

Configuration schema
====================

The configuration file is validated using JSON Schema you can find at
`here <https://grouperenault.gitlab.io/gitlab-project-configurator/jsonschema/config-schema-v1.json>`__


High level nodes
================

Your configuration will be splitted into different parts, organized under different
high level nodes (or even several files when using the ``include`` node).

.. code-block:: yaml

  # Define groups of variables
  variable_profiles:
    ...

  # Define groups of members
  member_profiles:
    ...

  # Define group of labels
  label_profiles:
    ...

  # Define rules for projects
  projects_rules:
    ...

  # Apply rules on projects
  projects_configuration:
    ...

  # Define rules for groups
  groups_rules:
    ...

  # Apply rules for groups
  groups_configuration:
    ...

If you want, you can split in different files by using the ``include`` keyword.
This will allow you to specify a list of files to import and "merge" into the
current files. ``project_rules`` and ``groups_rules`` are appended (since both
are lists), while the other nodes, being dictionaries, are "updated".

- ``variables.yaml``:

  .. code-block:: yaml

      variable_profiles:
        ...

- ``members.yaml``:

  .. code-block:: yaml

      member_profiles:
        ...

- ``project_rules01.yaml``:

  .. code-block:: yaml

      projects_rules:
        ...

- ``project_rules02.yaml``:

  .. code-block:: yaml

      projects_rules:
        ...

- ``project_config01.yaml``:

  .. code-block:: yaml

      projects_configuration:
        ...

- ``project_config02.yaml``:

  .. code-block:: yaml

      projects_configuration:
        ...

- ``global_config.yaml``:

  .. code-block:: yaml

      include:
        - variables.yaml
        - members.yaml
        - project_rules01.yaml
        - project_rules02.yaml
        - project_config01.yaml
        - project_config02.yaml

  The reconstructed ``global_config.yaml`` will have all the content of the
  other files "merged".


Project Rules
=============

Rules are defined under ``projects_rules`` (or ``groups_rules`` for groupe)
section.

Each rule must have a ``rule_name``.

GPC supports the following settings in the ``projects_rules`` sections:

Force default branch creation
-----------------------------

By using ``force_create_default_branch`` option you can force the creation of the default branch from
current default (if the branch does not exist) or create an initial commit if the repository is empty.

.. code-block:: yaml

    projects_rules:
      - rule_name: myteam_master_rule
        default_branch: master
        force_create_default_branch: true


Null handling
-------------

Available since version: 1.0

In most of the rules available in GPC, the default value is set to ``null``
which has a special meaning:

``null`` == "Don't touch !"

If you have some configuration defined in a project, and you don't want them
to be updated

.. code-block:: yaml

    projects_configuration:
      - paths:
          - groupname/my_project01
        rule_name: default_policy
        custom_rules:
          protected_tags: null

This shows how a rule named ``default_policy`` is applied on some project but
the protected tags are keps as they are, even if they are forced in the rule.

Rule inheritance
----------------

Available since version: 2.0

A rule can inherit from another one using the ``inherits_from`` keyword.

.. code-block:: yaml

    projects_rules:
      - rule_name: base_policy
        default_branch: master
        permissions:
          visibility: internal

      - rule_name: extended_policy
        inherits_from: base_policy
        permissions:
          visibility: private


In this example, ``extended_policy`` will have its ``visibility`` set to
``private`` but the ```default_branch``` will reuse the one from
``base_policy``.

More complex use case can be:

.. image:: ../../data/gpc-usecase.png

Multiple Rule inheritance
-------------------------

Available since version: 4.7.0

A rule can inherit from other rules using the ``inherits_from`` keyword.

.. code-block:: yaml

    projects_rules:
      - rule_name: first_base_policy
        default_branch: master
        permissions:
          visibility: internal
      - rule_name: scnd_base_policy
        permissions:
          visibility: private
        mergerequests:
          merge_method: ff

      - rule_name: extended_policy
        inherits_from:
          - first_base_policy
          - scnd_base_policy
        push_rules:
          remove: true

In this example, ``extended_policy`` will have its ``visibility`` set to
``private`` (because ``scnd_base_policy`` overrides rule ``first_base_policy``) but the ```default_branch``` will reuse the one from
``first_base_policy``.

This configuration is equivalent for ``extended_policy`` to:

.. code-block:: yaml

    projects_rules:
      - rule_name: extended_policy
        default_branch: master
        permissions:
          visibility: private
        mergerequests:
          merge_method: ff
        push_rules:
          remove: true

Executing a subset of the schema
--------------------------------

It is also possible to apply a subset of properties of your config schema. You need to use the ``--executor`` option and specify the property you want to apply (or a comma separated list of properties)
(project_members, members, protected_branches, protected_tags, variables, labels, approval_rules, mergerequests, approval_settings, jira, badges, pipelines_email, push_rules, runners, schedulers, deploy_keys).

.. code-block:: shell

    gpc --mode apply --executor members


.. important:: Please keep in mind that some properties have dependencies (for example you shouldn't apply your approval_rules configuration before your members configuration since some members might not have been added yet)

Using threadpools
-----------------

Some people need to configure a large number of projects. You can use ``--max-workers`` option to define the number of workers you want for threadpool execution.

.. code-block:: shell

    gpc --mode apply --max-workers 8


Project Permission Settings
---------------------------

Project Visibility
~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://gitlab.com/help/public_access/public_access#visibility-of-projects>`__

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        permissions:
          visibility: internal

Available value for ``visibility``:

- ``public``: Public projects can be cloned without any authentication.
- ``internal``: Internal projects can be cloned by any logged in user.
- ``private``: Private projects can only be cloned and viewed by project members
- ``null`` or undefined: do not change


Allow users to request access
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project>`__

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        permissions:
          request_access_enabled: true

- ``null`` or undefined: do not change

Enable/Disable Wiki, LFS, Snippets, Issues, CI/CD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#sharing-and-permissions>`__

Available since version: 2.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        permissions:
          wiki_enabled: false
          lfs_enabled: false
          issues_enabled: false
          snippets_enabled: false
          container_registry_enabled: false
          jobs_enabled: false
          merge_requests: false
          packages_enabled: false

Most of these settings are set to true per default on new projects.

- ``null`` or undefined: do not change

.. note:: Disabling Git LFS on the group may result in LFS option being not accessible.

Releases Access Level
~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        releases_access_level: enabled

Configure availability of releases on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Infrastructure Access Level
~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        infrastructure_access_level: enabled

Configure availability of infrastructure on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Feature Flags Access Level
~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        feature_flags_access_level: enabled

Configure availability of feature flags on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Environments Access Level
~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        environments_access_level: enabled

Configure availability of environments on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Monitor Access Level
~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        monitor_access_level: enabled

Configure availability of monitoring on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Pages Access Level
~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        pages_access_level: enabled

Configure availability of pages on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Analytics Access Level
~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        analytics_access_level: enabled

Configure availability of analytics on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Forking Access Level
~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        forks_access_level: enabled

Configure availability of forks for a project.

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Security and Compliance Access Level
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/settings/#project-feature-settings>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        security_and_compliance_access_level: enabled

Configure availability of Security and Compliance on a project.

Options include:

- ``enabled``
- ``disabled``
- ``private``
- ``null`` or undefined: do not change

Project Members
---------------

Available since version: 2.0

Example:

.. code-block:: yaml

    member_profiles:
      - name: mytest_master_approvers
        role: maintainers
        members:
          - calamity.desperados
          - path/to/your/group

    projects_rules:
      - rule_name: default_policy
        project_members:
          profiles:
            - mytest_master_approvers
          members:
            - role: developers
              name: calamity.desperados
            # list are also supported
            - role: maintainers
              names:
                - billy.thekid
                - buffalo.bill

      - rule_name: another_policy
        project_members:
          profiles:
            - mytest_master_approvers

You can define your project members in the section ``project_members`` by
a list of ``profiles`` or directly by a list of ``members``.

- ``profiles`` list of profiles defines in the ``member_profiles`` node,
  ie the members defined will have the role defined in the profile.
- ``members``: you can also define additionals members,
  configuring the name and the role.
  If your member is already set in the profile, the role is overrided.

In the previous example, the user ``calamity.desperados`` will have the role
``developers``.

.. note:: You can only add or remove direct members of a project,
    as long as they are not owner of the project.
    This means that members added in any group containing your project will
    keep their group rights.

Available roles are: ``reporters``, ``developers``, ``guests``, ``maintainers``,
``owners``.

Project Labels
--------------

Available since version: 1.0

.. code-block:: yaml

    label_profiles:
      - name: some_labels
        labels:
        - name: label_from_profile_1
          color: '#424242'
        - name: label_from_profile_2
          color: '#424242'

    projects_rules:
      - rule_name: default_policy
        labels:
          - name: label_name
            color: '#FF0000'
          - profile: some_labels

It is possible to define one or several project labels:

- ``name``: The label name
- ``color``: The label color.

It is also possible to define a label profile and apply it in project rules.

- ``profile``: The profile name

.. important:: Please note that existing labels are not removed,
  even if they are not defined in this confirugation.


Access Tokens Settings
----------------------

Enable and Limit CI_JOB_TOKEN access
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manage which projects can use their CI_JOB_TOKEN to access your project and select the projects that can be accessed by API requests authenticated with this project's CI_JOB_TOKEN CI/CD variable.

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        token_access:
          allow_access_with_ci_job_token: true
          limit_ci_job_token_access: true

Define the projects with access
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        token_access:
          allow_access_with_ci_job_token: true
          allowed_projects:
            - group/project1
            - group/project2
          limit_ci_job_token_access: true
          limited_projects:
            - group/project3
            - group/project4

Merge Requests Settings
-----------------------

Merge Method
~~~~~~~~~~~~
`Gitlab documentation <https://docs.gitlab.com/ee/user/project/merge_requests/#semi-linear-history-merge-requests>`__

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          merge_method: ff

Available values:

- ``ff``: Fast-forward merge
- ``merge``: Merge commit
- `rebase_merge`: Merge commit with semi-linear history
- ``null`` or undefined: do not change

Merge when pipeline succeeds
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`Gitlab documentation <https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html#only-allow-merge-requests-to-be-merged-if-the-pipeline-succeeds>`__

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          only_allow_merge_if_pipeline_succeeds: true

You can prevent merge requests from being merged if their pipeline did not succeed.

- ``true``: pipeline must succeed to be able to merge the MR
- ``false``: pipeline does not prevent the MR to be merged
- ``null`` or undefined: do not change

Merge only if all discussions are resolved
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`Gitlab documentation <https://docs.gitlab.com/ee/user/discussions/index.html#only-allow-merge-requests-to-be-merged-if-all-discussions-are-resolved>`__

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          only_allow_merge_if_all_discussions_are_resolved: true

You can prevent merge requests from being merged if there are discussions to be
resolved:

- ``true``: discussion must be resolved to be able to merge the MR
- ``false``: open discussion does not prevent the MR to be merged
- ``null`` or undefined: do not change

Automatically resolved outdated MR discussions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/discussions/index.html#automatically-resolve-merge-request-diff-discussions-when-they-become-outdated>`__

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          resolve_outdated_diff_discussions: true

Enable or Disable "Automatically resolve merge request diff discussions when
they become outdated" Merge Request Settings:

- ``true``: feature is enabled, merge request diff discussions on lines modified
  with a new push will be automatically resolved
- ``false``: feature is disabled, all merge request diff discussions need to be
  manually resolved.
- ``null`` or undefined: do not change

Merged results pipelines (premium only)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/ci/pipelines/merged_results_pipelines.html>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          merge_pipelines_enabled: true


Merge trains (premium only)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html>`__

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          merge_pipelines_enabled: true
          merge_trains_enabled: true


.. important:: To enable merge trains merged results pipelines must be enabled too

Show link to create/view MR when pushing a branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          printing_merge_request_link_enabled: true

Enable or Disable "Show link to create/view merge request when pushing from the
command line" Merge Request settings:

- ``true``: feature is enabled, when someone is pushing a branch to GitLab,
  GitLab will provide a link to the associated MR if it exists, or a link to
  create a new MR if the branch is pushed for the first time
- ``false``: feature is disabled, when someone is pushing a branch to GitLab,
  no link are provided.
- ``null`` or undefined: do not change

Remove source branch after merge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          remove_source_branch_after_merge: true

Enable or Disable "'Delete source branch' option by default" Merge Request
settings:

- ``true``: feature is enabled, when someone merges the merge request on gitlab,
  the source branch is removed after merge
- ``false``: feature is disabled.
- ``null`` or undefined: do not change

Merge Request Approval Settings
-------------------------------

Approvals required
~~~~~~~~~~~~~~~~~~

Available since version: 2.0

.. important:: Mandatory when configuring approvers

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        approvers:
          minimum: 1

You can set the minimum of approvals required for a merge request.

.. note:: This is a mandatory setting when configuring approvers.

Override approvers/approvals required per merge request
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab Documentation <https://docs.gitlab.com/ee/user/project/merge_requests/#semi-linear-history-merge-requests>`__

Available since version: 2.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        approvers:
          options:
            can_override_approvals_per_merge_request: true

You can prevent the update of approvers/approvals required per merge request.

- ``true``: Approvers and approval can be overridden per merge request
- ``false``: Approvers and approval can not be overridden per merge request
- ``null`` or undefined: do not change

Resetting approvals on push
~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#resetting-approvals-on-push>`__

Available since version: 2.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        approvers:
          options:
            remove_all_approvals_when_new_commits_are_pushed: false

You can define that all approvals of a merge request are removed when new
commits are pushed to the source branch.

- ``true``: All approvals are removed when new commits are push to the source branch.
- ``false``: The approvals are kept.
- ``null`` or undefined: do not change

Enable self-approval
~~~~~~~~~~~~~~~~~~~~

`Gitlab documentation <https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#allowing-merge-request-authors-to-approve-their-own-merge-requests>`__

Available since version: 2.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        approvers:
          options:
            enable_self_approval: false

You can define that authors of a merge request can approve their own merge
requests.

- ``true``: The author can approve its own merge request.
- ``false``: The author can not approve its own merge request.
- ``null`` or undefined: do not change


Enable Committers Approval
~~~~~~~~~~~~~~~~~~~~~~~~~~

`Gitlab Documentation <https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#allowing-merge-request-authors-to-approve-their-own-merge-requests>`__

Available since version: 2.5.4

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        approvers:
          options:
            enable_committers_approvers: false

You can define that committers of a merge request can approve the merge request.

- ``true``: A committer can approve its own merge request.
- ``false``:  A committer can not approve its own merge request.
- ``null`` or undefined: do not change

Approvers
~~~~~~~~~

`Gitlab Documentation <https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html>`__

Available since version: 2.0
Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        approvers:
          members:
            - some.user


You can define a list of approvers.
These approvers can be users (set the username), or groups (set the group path).

You can removed all approvers with an empty list: ``[]``.

This field can be used with a profile, to get a merged list of approvers.


Approval Profile
~~~~~~~~~~~~~~~~

**Import from a profile**

Available since version: 2.0
Example:

.. code-block:: yaml

    member_profiles:
      - name: mytest_master_approvers
        role: maintainers
        members:
          - someone
          - path/to/your/group

    projects_rules:
      - rule_name: my_rule_name
        approvers:
          profiles:
            - mytest_master_approvers

Available since version: 2.0

It is possible to define a list of profiles which contain a list of approvers.

These profiles can be used in several project rules,
and for users of protected branches too.

Note, that it is possible to set several profiles as approvers.

Note, in section ``member_profiles`` the ``role`` is not mandatory for
approvers.
But if you want use a profile in the section ``project_members`` you should
define the role.

Add several approval rules (premium only)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
It is possible to add more than one approval rule for your merge request (with gitlab premium).
You need to use ``approval_rules`` as it follows:

.. code-block:: yaml

    approval_rules:
      - name: rule1
        profiles:
          - profile1_approvers
        members:
          - some_member
        minimum: 2
        protected_branches:
          - master
      - name: rule2
        profiles:
          - another_profile
        members:
          - another_member
        minimum: 8
        protected_branches:
          - shadow
      - name: rule3
        minimum: 4
        protected_branches:
          - a_branch_approved_by_default_eligible_approver
      - name: Coverage-Check # inbuilt rule
        minimum: 4
        profiles:
          - profile1_approvers
        members:
          - some_member
      - name: License-Check # inbuilt rule
        minimum: 4
        profiles:
          - profile1_approvers
        members:
          - some_member

And you can set the approval settings with ``approval_settings``
(it is an alternative to ``options`` of ``approvers`` section) as it follows:

.. important:: spell correctly ``License-Check`` and ``Coverage-Check`` when setting inbuilt rules

.. code-block:: yaml

    approval_settings:
        can_override_approvals_per_merge_request: true
        remove_all_approvals_when_new_commits_are_pushed: false
        enable_self_approval: false
        enable_committers_approvers: false

.. important:: When using ``approval_rules`` and ``approval_settings`` it is important to make sure you're not using ``approvers`` section (and vice versa)

Repository Settings
-------------------

Default branch
~~~~~~~~~~~~~~

Available since version: 1.0

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        default_branch: master

Set default branch of the project.

Only one default branch is allowed per project.

If the wanted branch does not exist yet, GPC will not do anything and
**will not fail**.


Project description
~~~~~~~~~~~~~~~~~~~

Available since version: 2.0

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        description: my description

Set the description of project.



CI configuration file
~~~~~~~~~~~~~~~~~~~~~

Available since version: 3.4.0

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        ci_config_path: my_ci_config_file.yml

The path to CI configuration file.



Deploy keys
~~~~~~~~~~~

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        deploy_keys:
          - id: 123456 # ID of your deploy key
            can_push: true # whether or not the deploy key is allowed to push

Enable (already existing) deploy keys to your project



Auto-cancel pending pipelines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Available since version: 2.5.4

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        auto_cancel_pending_pipelines: enabled  # | disabled

Auto cancel the pending pipeline each time any pipeline is created.

Values: ``enabled`` or ``disabled``.



Commit message templates
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        merge_commit_template: Merge %{source_branch} into %{target_branch}
        squash_commit_template: |
            %{title}

            %{url}
            %{co_authored_by}

Define commit message templates.

`Gitlab Documentation <https://docs.gitlab.com/ee/user/project/merge_requests/commit_templates.html>`__


Issues Template
~~~~~~~~~~~~~~~

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        issues_template: Describe the issue.

Define issues template

`Gitlab Documentation <https://docs.gitlab.com/ee/user/project/description_templates.html>`__

Merge Request Template
~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        mergerequests:
          default_template: some string


Define issues' template

`Gitlab Documentation <https://docs.gitlab.com/ee/user/project/description_templates.html>`__


Build Coverage Regex
~~~~~~~~~~~~~~~~~~~~

Available since version: 2.5.4

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name

Set the regex to extract the coverage result of a project.

.. note:: You need to double you slash characters.

To remove the value, you can set empty string.

Protected branches
~~~~~~~~~~~~~~~~~~

`Gitlab Documentation <https://gitlab.com/help/user/project/protected_branches>`__

Available since version: 1.0, and extended in version 2.0

.. code-block:: yaml

    # non-extended version
    projects_rules:
      - rule_name: my_rule_name
        protected_branches:
          - pattern: master
            allowed_to_merge: maintainers
            allowed_to_push: none
            allow_force_push: false # optional
            code_owner_approval_required: true # optional

Defines the branches to protect.

- ``pattern``: can use wildcard to configure a set of branches.Please ensure
  to double-quote this value.

  .. code-block:: yaml

      protected_branches:
        - pattern: "stable/*"

- ``allowed_to_merge`` and ``allowed_to_push``: either a single role
  (GPC < 2.0), or a more structured configuration.

  Available roles:

  - ``maintainers``: only maintainers of the project can merge (or push, or
    both)
  - ``developers``: developers and maintainers of the project can merge (and/or
    push)
  - ``none``: No one is allowed to merge (or push, or both)

  You can either define a single role:

- ``allow_force_push`` and ``code_owner_approval_required``: can be optionally set to either `true`, `false`, or `null` where the `null`-value will always keep the set value provided by GitLab.

  - ``allow_force_push``: Allow all users with push access to force push
  - ``code_owner_approval_required``: Prevent pushes to this branch if it matches an item in the CODEOWNERS file

  .. code-block:: yaml

      allowed_to_merge: maintainers

  Or use a more complex structure to add individuals
  (available since version: 2.0):

  - ``role``: ``maintainers``, ``developers`` or ``none``
  - ``members``: List of user name or full path to group to add
    (ex: ``groupname/myteam/reviewers``)

    .. important:: The users and groups should be members of the project !

  - ``profiles``: List of profiles (define in a global section
    ``member_profiles``) which contain list of users and groups.

  .. note:: In the section ``member_profiles``, the ``role`` is not mandatory
    for approvers.
    But if you want use a profile in the section ``project_members``,
    you should define the role.

  Example of a complex approval (version >=2.0)

  .. code-block:: yaml

      member_profiles:
        - name: mytest_master_approvers
          role: maintainers
          members:
            - roger.duvel
            - path/to/group2

        - name: mytest_profiles
          role: maintainers
          members:
            - roger.duvel
            - ricardo.corona
            - path/to/group/profiles

    projects_rules:
      - rule_name: my_rule_name
        protected_branches:
          - pattern: master
            allowed_to_merge:
              role: maintainers
              profiles:
                - mytest_profiles
              members:
                - jean.kwak
                - path/to/group
            allowed_to_push: none
            allow_force_push: false # optional
            code_owner_approval_required: true # optional


.. note::

  ``protected_branches = null`` (the default) means GPC will not modify this
  value.

  ``protected_branches = []`` will remove all existing protected branches.

.. important:: if ``protected_branches`` is defined (has a non-``null`` value),
  all existing protected branches that are not configured in GPC will still
  be removed.


Protected tags
~~~~~~~~~~~~~~

`Gitlab documentation <https://gitlab.com/help/user/project/protected_tags>`__

Available since version: 1.0

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        protected_tags:
          - pattern: "master"
            allowed_to_create: maintainers

Protected Tags allows controling who has permission to create tags as well as
preventing accidental update or deletion of tags once created.

Available configurations:

- ``pattern``: can use wildcard (``*``) to configure a set of tags.
  Do not forget to add double quote.

  Example:

  .. code-block:: yaml

      pattern: "stable/*"

- ``allowed_to_create`` can be either of the following values:

  - ``maintainers``: only maintainers of the project can create / update or
    delete tags
  - ``developers``: developers and maintainers of the project can create /
    update or delete tags
  - ``none``: No one is allowed to create / update or delete tags.

As of Gitlab 12.4, the Gitlab API does not provide a way to set individuals
in the ``protected_tags`` while it is possible to do it manually in the UI.

Upstream issue:
`gitlab#7018 <https://gitlab.com/gitlab-org/gitlab/issues/7018>`__.

.. important:: if ``protected_tags`` is defined (has a non-``null`` value),
  all existing protected tags that are not configured in GPC will still be
  removed.

Project Git Shallow Clone
~~~~~~~~~~~~~~~~~~~~~~~~~

Available since version: 2.5.1

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        ci_git_shallow_clone: 50

Set the property Git Shallow Clone, ie the maximum number of changes to be
fetched from GitLab when cloning a repository.
This can speed up Pipelines execution.

Set to ``0`` to disable shallow clone by default and make GitLab CI fetch all
branches and tags each time.

CI / CD Settings
----------------

Environment Variables
~~~~~~~~~~~~~~~~~~~~~

`Gitlab Documentation <https://docs.gitlab.com/ee/ci/variables/README.html#variables>`__

Available since version: 1.0, extended in version 2.2.0 for `masked` variables.

Example:

.. code-block:: yaml

    projects_rules:
      - rule_name: my_rule_name
        variables:
          - name: VARIABLE_NAME
            value: value for VARIABLE_NAME
            protected: true
            variable_type: env_var
          - name: MY_VARIABLE_MASKED
            value: masked_variable
            variable_type: env_var
            masked: true  # added in 2.2.0

Allow to configure a variable and its value directly in the configuration file.

Possible settings:

- ``name``: name of the variable. Prefer upper-case.
- ``value``: value given to the variable, this is set directly in the
  configuration file.
- ``value_from_envvar``: take the value from an environment variable defined in
  your config project.
- ``protected`` (optional boolean, default: ``False``): define if this is going
  to be a protected variable or not.
  Protected variables is only available on protected branches.
- ``variable_type`` (optional): Type of variable, default (```env_var```) is a
  classic environnement variable , or a file (``file``) can be specified
  (`see here <https://docs.gitlab.com/ee/ci/variables/#variable-types>`__).
- ``masked`` (optional boolean, default: False): define if this is going to be
  a masked variable or not.
  Note the value must match a regular expression: (see the
  `Masked Variables documentation <https://docs.gitlab.com/ee/ci/variables/README.html#masked-variables>`__).

  If you want GPC to check the value of your variable using the same regular
  expression, you need to use either ``value`` or ``value_from_envvar`` with
  and unprotected variable in the current config project (but the target
  variable can be protected).

.. important:: It is not possible to define or to use a variable starting by a
  numeric value! All variables must start with alphabetic characters.


Variables Profiles
~~~~~~~~~~~~~~~~~~

Available since version: 1.0,

Example:

.. code-block:: yaml

    variable_profiles:
      COMMON_VARIABLES:
        # Either, set variables directly
        # from this yml file
        - name: SOME_VARIABLE_NAME
          value: some variable value
          protected: true
        # or set variables from environment
        # variables of this project
        - name: PRIVATE_VARIABLE
          value_from_envvar: SOMETHING_PRIVATE
          protected: true

    projects_rules:
      - rule_name: default_project_policy
        variables:
          - import: COMMON_VARIABLES

      - rule_name: another_policy
        variables:
          - import: COMMON_VARIABLES

It is possible to define one or several variables in a "variable profile"
and import it in rules with the ``import`` keyword.

This is useful for instance to propagate several variables logically linked like
user and token.

Runners
~~~~~~~

Available since version: 2.0

.. code-block:: yaml

    runners:
      - runner_id: 666
        enabled: true

It is possible to enable or disable runners for a profile.

These runners should be specific runners.
It is not possible to enable/disable shared runners.
If you define a shared runner, a warning message will display,
but the specific runners will be updated.

If you have paused runner, GPC is not able to resume it.
If you enable a paused runner, this runner will be enable but in pause.

Schedules
~~~~~~~~~

Available since version 2.1.0

.. code-block:: yaml

    schedulers:
      - name: schedule_toto
        branch: master
        cron: "0 4 1 * *"
        enabled: true
        tz: UTC # by default
        variables:
          - import: SET_OF_VARIABLES
          - name: OTHER_VARIABLE
            value: other value
          - name: var_2
            value_from_envvar: ENVVAR_VALUE

It is possible to define schedulers.

You need to defines theses fields:

* ``name``: The name of scheduler
* ``branch``: the reference of the scheduler
* ``cron``: The interval pattern as cron syntax
* ``enabled``: The pipeline schedule is active or not
* ``tz``: The cron timezone
* ``variables``: You can define varibles for this pipelines.
  For that you can use the same format than "Environment variables" section.

Project Setting
---------------

Project Badges
~~~~~~~~~~~~~~

`Gitlab document <https://docs.gitlab.com/ee/user/project/pipelines/settings.html#pipeline-badges>`__

Available since version: 2.0

Usage:

.. code-block:: yaml

    badges:
      # Pipeline Status badge
      - name: pipeline
        link_url: "%{gpc_gitlab_url}/%{project_path}/commits/%{default_branch}"
        image_url: "%{gpc_gitlab_url}/%{project_path}/badges/%{default_branch}/pipeline.svg"
      # Coverage Status badge
      - name: coverage
        link_url: "http://some.server/path/%{project_path}/%{default_branch}/coverage/index.html"
        image_url: "%{gpc_gitlab_url}/%{project_path}/badges/%{default_branch}/coverage.svg"

By default, all projects modified by GPC has the following badge:

.. image:: images/gpc-badge.png

You can add you custom badges. Each configuration simply takes a name and two
URL:

* ``name``: name of the badge
* ``image_url``: URL to the badge (Gitlab only provides pipeline and coverage
  badges automatically)
* ``link_url``: URL to open on click

You have the following variables available in ``link_url`` or ``image_url``:

.. list-table:: Title
    :widths: 25 25 50
    :header-rows: 1

  * - Variable name
    - Description

  * - ``%{gpc_gitlab_url}``
    - URL to Gitlab

  * - ``%{project_path}``
    - path to the project being configured

  * - ``%{default_branch}``
    - default branch on the project being configured

Artifacts
~~~~~~~~~

`Gitlab document <https://docs.gitlab.com/ee/api/projects.html#edit-project>`__

Disable or enable the ability to keep the latest artifact for this project.

Usage:

.. code-block:: yaml

    artifacts:
      keep_latest_artifact: false

Integrations
------------

Jira
~~~~

Available since version: 2.0

.. code-block:: yaml

    integrations:
      jira:
        url: 'https://my.jira.server
        jira_issue_transition_id: 101
        disabled: false
        username: my bot
        password_from_envvar: ENVVAR_PASSWORD
        trigger_on_commit: false
        trigger_on_mr: false
        comment_on_event_enabled: false

You can configure the Jira service for your project.

You need to set:

- ``url``
- ``jira_issue_transition_id`` (optional) :

  - This value can be 'null', an empty string, an integer,
    or multipe intergers separated by **,** or by **;**
  - If this value is not set, we keep the server value
  - If the value is an empty string => We remove the server value

- ``username``: the username of the account that will perform the action
- ``username_from_envvar``: the username is stored in an environment variable
- ``password_from_envvar``: This password is stored in an environment variable
- ``trigger_on_commit``: Add a link in JIRA pointing to commit referencing the
  JIRA
- ``trigger_on_mr``: Add a link in JIRA pointing to merge request referencing the
  JIRA (defaul: ``true``)
- ``comment_on_event_enabled``: Enable comments inside Jira issues on each GitLab
  event (commit / merge request / tag / rebase / etc...) (default: ``False``)
- ``disabled``: It is an optional field. The default value is ``false``.
  If you want to disabled the Jira service, you set this field to 'true',
  and GPC removes the Jira service configuration.

Pipeline Emails
~~~~~~~~~~~~~~~

.. code-block:: yaml

    integrations:
      pipelines_email:
        recipients:
          - user1.name@someserver.com
          - user2.name@otherserver.com
        notify_only_broken_pipelines: false
        notify_only_default_branch: true
        pipeline_events: true

You can configure notification of pipelines for your project.

You need to set:

- ``recipients``: List of recipient email addresses
- ``notify_only_broken_pipelines`` (optional): Notify on broken pipelines
- ``notify_only_default_branch`` (optional): Send notifications only for the
  default branch
- ``pipelines_events`` (optional): Enable notifications for pipeline events
- ``disabled`` (optional): Remove pipelines email services


Push rules
~~~~~~~~~~

`Gitlab Documentation <https://gitlab.com/help/push_rules/push_rules.md#push-rules-starter>`__

.. code-block:: yaml

    push_rules:
      remove: false
      dont_allow_users_to_remove_tags: true
      member_check: true
      prevent_secrets: true
      commit_message: '^(.+)$'
      commit_message_negative: 'truc'
      branch_name_regex: '^(.+)$'
      author_mail_regex: '^(.+)$'
      prohibited_file_name_regex: '^(.+)$'
      max_file_size: 500
      reject_unsigned_commits: false

You can configure the push rules for yout project

You need to set:

- ``dont_allow_users_to_remove_tags``: User can not removed tag with git push
- ``member_check``: Check if author is a gitlab user.
- ``prevent_secrets``: Reject anu files that are likely to contain secrects
- ``commit_message``: Regex for commit message
- ``commit_message_negative``: No commit message is allowed to match this regex
  to be pushed.
- ``reject_unsigned_commits``: Only signed commits can be pushed to this repository.
- ``branch_name_regex``: Regex for branch name
- ``author_mail_regex``: Regex for author mail
- ``prohibited_file_name_regex``: Filename must not match this regex to be
  pushed
- ``max_file_size``: Pushes that contain added or updated files that exceed this
  file size (MB) are rejected.
- ``remove``: Optional, you want removed all the push rules settings previously
  configured.

Keep options
------------

Available since version: 2.4, extended in 2.5.6

Some specific ``keep_...`` keywords exist to keep existing projects
configuration that are defined in lists.

Here are the exhaustive list:

- ``keep_existing_schedulers``
- ``keep_existing_protected_branches``
- ``keep_existing_protected_tags``
- ``keep_existing_variables``
- ``keep_existing_members``

Those keywords allow to add new schedulers or protected branches or tags or
variables, without erasing existing values.

.. code-block:: yaml

    keep_existing_schedulers: true
    schedulers:
      - name: schedule_toto
        branch: master
        cron: "0 4 1 * *"
        enabled: true
        tz: UTC  # by default
        variables:
          - name: OTHER_VARIABLE
            value: other value
          - name: var_2
            value_from_envvar: ENVVAR_VALUE

    keep_existing_protected_branches: true
    protected_branches:
      - pattern: master
        allowed_to_merge:
          role: maintainers
          profiles:
            - mytest_profiles
          members:
            - robert.tripel
            - jean.kwak
        allowed_to_push: maintainers

    keep_existing_protected_tags: true
    protected_tags:
      - pattern: master
        allowed_to_create: maintainers
        keep_existing_members: true
        project_members:
          members:
            - role: developers
              name: user_toto

    keep_existing_variables: true
    variables:
      - name: ANOTHER_VARIABLE_NAME
        value: another value

In this exemple, as the keep options are set to true,
existing projects variables are going to be kept,
and the project members too.

If set to false, projects configuration will be overwritten with specified
GPC configuration.

For example, if ``keep_existing_variables`` is set to true and one of the
specified variables already exists in the project, it will be overwritten!

Project members
---------------

Sometimes you try to add a member with maintainer rights to a project where he is
already an owner (inherited from a group). Default behavior raises a `GpcMemberError`.
In this particular case you can enable `skip_permission_error` to skip this user.

.. code-block:: yaml

    projects_rules:
  - rule_name: myteam_master_rule

    default_branch: master
    keep_existing_members: true
    skip_permission_error: true
    project_members:
      members:
        - role: maintainers
          name: some.user
        - role: maintainers
          name: another.user

Groups Rules
============

This allows to change some settings for the group.
This is configured under the ``groups_rules`` node.

Group Members
-------------

Available since version: 2.5.5

**Usage**:

.. code-block:: yaml

    member_profiles:
      - name: mytest_master_approvers
        role: maintainers
        members:
          - calamity.desperados
          - path/to/your/group

    groups_rules:
      - rule_name: default_policy
        group_members:
          profiles:
            - mytest_master_approvers
          members:
            - role: developers
              name: calamity.desperados
            # list are also supported
            - role: maintainers
              names:
              - billy.thekid
              - buffalo.bill


You can define your group members, configuring in the section
``group_members`` a list of ``profiles`` or a list of ``members``

- ``profiles``: list of profiles defines in the ``member_profiles`` node,
  the members defined will have the role defined in the profile
- ``members``: you can also define additionals members,
  configuring the name and the role.
  If your member is already set in the profile, the role is overrided by the
  member.

In the previous example, the user ``calamity.desperados`` will have the role
``developers``.

Available roles are: ``reporters``, ``developers``, ``guests``, ``maintainers``,
``owners``.

Group Variables
---------------

Available since version: 2.5.0

See the Project variable for more information.

Group variables support variable profiles and all other options available as
Project Variable.

Example:

.. code-block:: yaml

    variable_profiles:
      COMMON_VARIABLES:
        # Either, set variables directly
        # from this yml file
        - name: SOME_VARIABLE_NAME
          value: some variable value
          protected: true
        # or set variables from environment
        # variables of this project
        - name: PRIVATE_VARIABLE
          value_from_envvar: SOMETHING_PRIVATE
          protected: true

    groups_rules:

      - rule_name: default_group_policy
        keep_existing_variables: true
        variables:
          - import: COMMON_VARIABLES
          - name: A_VARIABLE_NAME
            value: a_direct_value
            protected: true
          - name: SOME_PASSWORD
            value_from_envvar: SOME_PASSWORD
            protected: true
            masked: true

Configurations
==============

"Configurations" are application of the rules on projects or groups.
Because there could be some good (and also bad) reasons vary from the rule,
there is the ability to apply "customization" on a project (or group of
projects), ie change something from the rule the project is following.

It is up to the maintainer to understand weither this variation is worth the
higher maintainance cost.

Projects configuration
----------------------

The ``projects_configuration`` section allows to select which policy to apply
on which project.

Example
~~~~~~~

For example:

.. code-block:: yaml

    include:
        # your policies can be splitted into several files
        - policies.yml
        - policies_1.yml

    projects_configuration:
      # It is possible to configure a list of projects
      - paths:
        - groupname/my_project01
        - groupname/my_project02
        rule_name: default_policy

      # or groups of projects, need to use recursive keyword for that
      - paths:
        - groupname/my_subgroup01
        - groupname/my_subgroup02
        recursive: true
        rule_name: another_policy

      # You can use regular expressions by starting the line with ^
      # and ending with $.
      # Do not forget to escape every "/" character.
    - paths:
        - ^groupname\/some\/path\/.*_to_include$
        recursive: true
        rule_name: another_policy

For each project, or group of projects, you can also decide to override the
defined policy in order to apply a ``custom_rules``:

.. code-block:: yaml

    include: policies.yml

    projects_configuration:
      - paths:
          - groupname/my_project01
        rule_name: default_policy
        custom_rules:
          variables: null  # if the policy wanted to change add some variables,
                           # we actually want to keep existing variables for
                           # this project
          mergerequests:
            only_allow_merge_if_all_discussions_are_resolved: false

      - paths:
          - groupname/my_project02
        rule_name: default_policy
        custom_rules:
          protected_tags: null  # ignore change on protected tags
          mergerequests:
            merge_method: rebase_merge  # force rebase merge
            only_allow_merge_if_pipeline_succeeds: false

Available since version: 4.7.0

For each project, or group of projects, you can also decide to use multiple policies that will be merged before apply:

.. code-block:: yaml

    include: policies.yml

    projects_configuration:
      - paths:
          - groupname/my_project01
        rule_name:
          - default_policy
          - another_policy

You can also define a specific rule for specific project, without create a rule,
using  ``custom_rules`` without ``rule_name``:


.. code-block:: yaml

    include: policies.yml

    projects_configuration:
      - paths:
          - groupname/my_project01
        custom_rules:
          mergerequests:
            merge_method: rebase_merge  # force rebase merge
            only_allow_merge_if_pipeline_succeeds: false


Project inclusion and exclusion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Usage:

.. code-block:: yaml

    projects\_configuration:
      - paths:
        - groupname/my_group01
        - groupname/my_group02
        excludes:
          - ^.*demo$
          - ^groupname\/my_group01\/test_.*$
        recursive: true
        rule_name: another_policy

The ``paths`` key defines the project or groups where the rule will be applied.

You can use full group or project paths, or a regular expression.
Regular expressions must start by ``^`` and end with ``$``.

.. important:: Do not forget to escape each slash ``/`` in your regex !

  For the project path ``"a/path"``, its regular expression will become
  ``"^a\/path$"``

By default, the project listing is not recursive. Enable subgroups by defining
``recursive: true``.

It is also possible to exclude some projects from a configuration using the
``excludes`` keyword.

If you want to exclude a complete subgroup, use the following pattern:

.. code-block:: yaml

      - paths:
        - groupname/my_group
        excludes:
          - ^groupname\/my_group\/test_.*$

The given example will configure all the projects included in groups `my_group`
except the projects having a name starting by ``test_``.

Apply on all projects
~~~~~~~~~~~~~~~~~~~~~

You can also apply the defined policy to all projects for which your user has
access.

For that you need to define "/" to the "paths" field.

.. code-block:: yaml

    include: policies.yml

    projects_configuration:
      - paths:
          - /
        rule_name: default_policy
        custom_rules:
          variables: null  # null means "keep them as they are"
          mergerequests:
            only_allow_merge_if_all_discussions_are_resolved: false


Most values described here takes as default value ``null`` which means
"do not touch it" for GPC.

For lists,``null`` still means "do not touch", but you need to know that an
empty list (ex: ``approvers: []``) means "remove everything".

Customization
-------------

From time to time, there can be the need to apply a rule but still exclude one
or two settings, or force some value. This can be seen as a bad practice.

For example:

.. code-block:: yaml

    projects_configuration:
      - paths:
          - some/project
        rule_name: default_policy
        custom_rules:
          protected_tags: null  # ignore change on protected tags
          mergerequests:
            merge_method: rebase_merge  # force rebase merge

On the project ``some/project``, we want to apply the rule ``default_policy``
but do not change the ``protected_tags`` settings, and force the merge method
to ``merge_rebase``, even if they are differently configured in the rules.

"Not seen yet"
--------------

Available since version: 1.0

By default, GPC is pretty direct: each configuration is executed one after the
other, so nothing prevents by default applying a rule twice on the same project.

Do can disable this behavior by setting ``not_seen_yet_only: true``
to exclude already configured projects.

This can be useful, for example, to apply a single rule to every project in a
group except one:

.. code-block:: yaml

    projects_configuration:
      - paths:
          - groupname/my_group01/my_project05
        rule_name: exception_policy

      - paths:
            - groupname/my_group01
        recursive: true
        not_seen_yet_only: true
        rule_name: general_policy

This shows how to apply ``general_policy`` on all projects under
``groupname/my_group01`` except ``groupname/my_group01/my_project05`` which
should follow ``exception_policy``.

Groups configuration
--------------------

Available since version: 2.5.5

As for projects configuration a specific sections is available to configure
groups.

.. code-block:: yaml

    include:
        - policies.yml
        - policies_1.yml

    groups_configuration:
      # It is possible to configure a list of groups
      - paths:
        - groupname/my_group01
        - groupname/my_group02
        rule_name: default_policy
