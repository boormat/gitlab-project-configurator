Releases
========

The main vehicule to execute GPC on your projects is by using the docker images
directly from your Gitlab-CI file.

Docker image
------------

Usage:

.. code:: yaml

    apply:
      # use "latest" or "unstable" label for cutting edge versions
      image: registry.gitlab.com/grouperenault/gitlab-project-configurator:latest

      # use "stable" label for released version
      image: registry.gitlab.com/grouperenault/gitlab-project-configurator:stable

.. warning::

    Please keep in mind we do not maintain old releases,
    for example there is no bug fixes or backport to v1 once development on v2 has been released.
    Branching v2 during the development of v3 for example is an option,
    but we do not want to spend effort on maintaining too many different versions of GPC.

In short, bug fixes will be done preferably on ``HEAD`` and released with a new version.

Several "channels" of images are provided:

.. flat-table::
  :header-rows: 1
  :widths: 2 1 2

  * - Docker image channels
    - Examples
    - Description

  * - :rspan:`1` Unable/Latest labels
    - ``unstable``
    - :rspan:`1` Follow development on "master"

  * - ``latest``

  * - Stable labels
    - ``stable``
    - Tag released, happens on end of EPIC.

  * - :rspan:`3` Semantic version releases
    - ``1.0.0``
    - :rspan:`3` Labels to each stable versions. |br|
      Unstable (development) versions |br|
      are **not** labelised with semver.

  * - ``1.1.0``
  * - ``1.1``
  * - ``1``

  * - :rspan:`2` Date version |br|
      (following `calver <https://calver.org/>`__ convention |br|
      with ``c`` + ``YY.[MM.[DD]]`` format)
    - ``c19.04.10``
    - :rspan:`2` Published after each successful build . |br|
      Example: `c19.10` to use the latest build |br|
      for October 2019.

  * - ``c19.04``
  * - ``c19``


.. https://stackoverflow.com/questions/54001054/force-a-new-line-in-sphinx-text

.. |br| raw:: html

    <br>
