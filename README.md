# Quick Guide
`Gitlab Project Configurator`: One project to configure them all!

![GPC](data/gpc-icon.png)

> Alias: **Gitlab Project Settings As Code**

* [GPC Source Code](https://gitlab.com/grouperenault/gitlab-project-configurator)
* [Coverage Report](https://grouperenault.gitlab.io/gitlab-project-configurator/coverage/index.html)
([coverage on Gitlab Pages](https://groupname.gitlab-pages.dt.renault.com/nestor/infra/gitlab-project-configurator/coverage/))
* [Online Technical Documentation](https://grouperenault.gitlab.io/gitlab-project-configurator/docs/index.html)
* [Example Configuration Project](https://gitlab.com/grouperenault/gpc_demo/gpc-demo-config/)
* [Configuration File Schema](https://grouperenault.gitlab.io/gitlab-project-configurator/jsonschema/config-schema-v1.json)
* [Medium Article presenting GPC](https://medium.com/grouperenault/introducing-gitlab-project-configurator-daa02b79183c)

## Features

* Configure n projects from one or several configuration files stored in a single project
* Configuration can be splitted into n files
* Profiles of settings can be used to refactorize the configuration.

Each project individually by defining all the parameters,
but GPC goes much further:
You can factorize the configurations of your project into profiles and even
inherit a profile from another one.
Also, because some projects might have good (or bad reasons) to differ from the
wanted profile, customizations can be made at the project level.

For example:
- project `MyPythonApp` follows the rules defined for all Python projects,
  named `our_python_projects`.
- We want to factorize some settings, for instance the "Fast-Forward Merge Commit", in all our
  projects, so `our_python_projects` inherits from `our_projects` profile.
- But for some reason, I need to add a specific setting on `MyPythonApp`, for instance a special
  variable, so it will have a customization.

![GPC Use Case](data/gpc-usecase.png)

GPC can be used to store some variables as Project's Secret and distribute
directly only to the projects that need those variables:

![GPC Workflow](data/gpc-workflow.png)

## Preview mode

GPC has a preview mode to enable potentially breaking changes. It is enabled
when you define `--preview` parameter or the GPC_PREVIEW environment variable.

It does the following changes:

- during inheritance resolution, "list" are extended instead of replaced. For
  example, is base rule define a list of variables, inherited rules will
  automatically have these rules and the one they might define.

  In this mode, there is no way to "remove" items from lists
  (ex: variables, protected_branches, protected_tags,...) in inherited rules.
  If you need to do you, you must rework your rules inheritance hierarchy.

## Usage

Create a new **PRIVATE** project (named hereafter `path/to/a/group/project-config`),
with the following files:

* Define this workflow in your `.gitlab-ci.yml`:

```yaml
stages:
  - test
  - apply

image: registry.gitlab.com/grouperenault/gitlab-project-configurator:latest

variables:
  # Please define GPC_GITLAB_TOKEN in your project, equivalent of --gitlab-token,
  # with a token to a user with "maintainer" access
  GPC_COLOR: force  # equivalent of '--color force'
  GPC_CONFIG: my_config.yml  # equivalent of '--config my_config.yml'
  GPC_REPORT_FILE: report-gpc.json # Equivalent of '--report-file report-gpc.json'
  GPC_REPORT_HTML: report-gpc.html
  GPC_DUMP_MERGED_CONFIG: merged-config-gpc.html
  GPC_PREVIEW: "1"  # enable breaking changes


dry_run:
  stage: test
  script:
    - gpc --diff
  artifacts:
    paths:
      - $GPC_REPORT_FILE
      - $GPC_REPORT_HTML
    expire_in: 30 days


dry_run:verbose:
  stage: test
  script:
    - gpc --verbose
  artifacts:
    paths:
      - $GPC_REPORT_FILE
      - $GPC_REPORT_HTML
    expire_in: 30 days


apply:
  stage: apply
  only:
    - master
    - schedules
    - triggers
  script:
    - gpc --mode apply
  artifacts:
    paths:
      - $GPC_REPORT_FILE
      - $GPC_REPORT_HTML
    expire_in: 30 days
```

* Define your policies in `common_policies.yml`:

```yaml
variable_profiles:
  # Upload to release and protected path on Artifactory
  SOME_PROTECTED:
    - name: SOME_PROTECTED_VALUE
      value: some_value
      protected: true
    - name: SOME_PROTECTED_PASSWORD
      value_from_envvar: SOME_PROTECTED_PASSWORD
      protected: true
      masked: true

member_profiles:
  - name: mytest_master_approvers
    role: maintainers
    members:
      - roger.duvel
      - path/to/a/subgroup
  - name: mytest_profiles
    role: developers
    members:
      - roger.duvel
      - ricardo.corona

projects_rules:
  ##############################################################################
  ## DEFAULT POLICY FOR ALL PROJECTS
  ##############################################################################
  - rule_name: default_policy

    default_branch: master
    force_create_default_branch: true # this will force default branch creation not existent and create an initial commit if empty repo

    description: the project description
    project_members:
      profiles:
        - mytest_master_approvers
        - mytest_profiles
      members:
        - role: developers
          name: toto.desperados
        - role: developers
          name: roger.duvel

    keep_existing_schedulers: true
    schedulers:
        - name: schedule_toto
          branch: master
          cron: "0 4 1 * *"
          enabled: true
          tz: UTC # by default
          variables:
              - import: ANY_VARIABLE
              - name: OTHER_VARIABLE
                value: other value
              - name: var_2
                value_from_envvar: ENVVAR_VALUE

    approval_rules:
    # Alternative for "approvers" section, it allows you to set multiple merge request approvals rules (premium)
        - name: rule1
          profiles:
            - mytest_master_approvers
          members:
            - roger.duvel
          minimum: 2
          protected_branches:
            - some_branch
        - name: rule2
          profiles:
            - mytest_master_approvers
          members:
            - regis.duval
          minimum: 1
          protected_branches:
          # you can enter a protected branch id or a branch name
            - another_branch

    approval_settings:
    # Alternative to "options" of "approvers" section, make sure not to use both
      can_override_approvals_per_merge_request: true
      remove_all_approvals_when_new_commits_are_pushed: false
      enable_self_approval: false
      enable_committers_approvers: false

    approvers:
    # To use only when there's one unique merge request approval rule
          profiles:
            - mytest_master_approvers
          members:
            - roger.duvel
          minimum: 2
          options:
            can_override_approvals_per_merge_request: true
            remove_all_approvals_when_new_commits_are_pushed: false

    keep_existing_protected_branches: false
    protected_branches:
      - pattern: master
        allowed_to_merge:
            role: maintainers
            profiles:
                - mytest_profiles
            members:
                - robert.tripel
                - jean.kwak
        allowed_to_push: maintainers
        allow_force_push: false
        allowed_to_unprotect: maintainers # developers and admins are also allowed, if not specified maintainers will be set to avoid 'no access'
        code_owner_approval_required: true

    keep_existing_protected_tags: false
    protected_tags:
      - pattern: master
        allowed_to_create: maintainers

    permissions:
      visibility: private
      request_access_enabled: true
      wiki_enabled: false
      issues_enabled: false
      snippets_enabled: false
      lfs_enabled: false

    mergerequests:
      only_allow_merge_if_all_discussions_are_resolved: true
      only_allow_merge_if_pipeline_succeeds: true
      resolve_outdated_diff_discussions: false
      printing_merge_request_link_enabled: true
      merge_method: ff
      squash_option: do not allow # set squash commits when merging default behavior from {do not allow, allow, encourage, require}
      merge_pipelines_enabled: false # premium only
      merge_trains_enabled: false # premium only (if true merge_pipelines_enabled must be true too)

    keep_existing_variables: false
    variables:
      # Import these chunk of variables from profiles defined in variable_profiles:
      - import: ANY_VARIABLE
      - import: SOME_PROTECTED
      # You can add other variables here:
      - name: ANOTHER_VARIABLE_NAME
        value: another value
      - name: A_HIDDEN_VARIABLE
        value_from_envvar: SOME_SECRET_NAME   # <= name of a variable secret in the config project
        masked: true
        protected: true

    runners:
      - runner_id: 666
        enabled: true

    integrations:
      jira:
        url: 'https://jira.server'
        jira_issue_transition_id: 101,102
        username: my bot # username can be defined with an environment variable using  username_from_envvar instead
        disabled: false
        password_from_envvar: JIRA_PASSWORD  # <= name of a variable secret in the config project
        trigger_on_commit: true

    push_rules:
        remove: false
        dont_allow_users_to_remove_tags: true
        member_check: true
        prevent_secrets: true
        commit_message: '^(.+)$'
        commit_message_negative: 'truc'
        branch_name_regex: '^(.+)$'
        author_mail_regex: '^(.+)$'
        prohibited_file_name_regex: '^(.+)$'
        max_file_size: 500
        reject_unsigned_commits: false

    nestor:
      enable: true

  - rule_name: default_internal_policy
    inherits_from: default_policy
    permissions:
      visibility: internal
```

* Add a configuration file to apply these profiles `my_config.yml`:

```yaml
include: common_policies.yml

projects_configuration:

  ##################################################################################################
  # 'Config' project: default policy, but keep it private and does not override the variables
  ##################################################################################################
  - paths:
      - path/to/a/group/project-config
    custom_rules:
      variables: null  # Do not override the variables for Config project
      permissions:
        visibility: private  # Keep the Config project private for security sake
    rule_name: default_policy

  ##################################################################################################
  # Default policy of the main projects
  ##################################################################################################
  - paths:
      - path/to/a/project
      - path/to/another/project
      - path/to/a/group
    excludes:
      - path/to/a/group/excluded/project
      - ^path\/to\/exclude\/with\/a\/.*regex$
    rule_name: default_policy
    not_seen_yet_only: true  # path/to/a/group/project-config will be excluded from this conf, there is a custom profile.
    recursive: true
    custom_rules:
      variables:
        - import: SOME_PROTECTED
```

You need to set the following environment variable:

* `GPC_GITLAB_TOKEN` (**not protected**): the token that will be used to query the Gitlab API

* every value using `value_from_envvar` (if **protected**, they will be not tested during the "dry-run")
  * `MY_PROTECTED_VARIABLE`
  * `ANOTHER_VARIABLE`

* Add a schedule on branch `master` every 3 hours at mid-hour: `30 */3 * * *`.

**Note**: ensure the policy on your Config project is "private".
You may leak unwanted tokens (especially the Gitlab token) if you keep the
project "internal" or even public.

## Contributing

Install:

```bash
make dev
```

Execute static analysis, code formating and unit tests:

```bash
make style check tests  # or make sct
```

Update the dependencies

```bash
$ make update
```
