"""
Conftest
----------------------------------
"""

# Third Party Libraries
import pytest


# pylint: disable=redefined-outer-name


@pytest.fixture
def fake_group(mocker):
    fg = mocker.Mock(name="Fake Gitlab Group")
    fg.web_url = "https://fake.gitlab.server/fake/group/path"
    fg.name = "Fake Group"
    fg.path = "fake/group/path"
    fg.full_path = "fake/group/path"
    fg.description = "My Fake Group"
    fg.default_branch = "old_default_branch"
    fg.visibility = "old_visibility"
    fg.request_access_enabled = False
    fg.runners_token = "ba324ca7b1c77fc20bb9"  # nosec


@pytest.fixture
def fake_project(mocker):
    fp = mocker.Mock(name="Fake Gitlab Project")
    fp.web_url = "https://fake.gitlab.server/fake/project/path"
    fp.default_branch = "old_default_branch"
    fp.visibility = "old_visibility"
    fp.request_access_enabled = False
    fp.wiki_enabled = True
    fp.lfs_enabled = True
    fp.jobs_enabled = True
    fp.merge_requests_enabled = True
    fp.archived = False
    fp.issues_enabled = True
    fp.snippets_enabled = True
    fp.packages_enabled = True
    fp.infrastructure_access_level = "Disabled"
    fp.releases_access_level = "Disabled"
    fp.feature_flags_access_level = "Disabled"
    fp.environments_access_level = "Disabled"
    fp.monitor_access_level = "Disabled"
    fp.pages_access_level = "Disabled"
    fp.analytics_access_level = "Disabled"
    fp.forking_access_level = "Disabled"
    fp.security_and_compliance_access_level = "Disabled"
    fp.container_registry_enabled = True
    fp.keep_latest_artifact = True
    fake_badges = mocker.Mock(name="fake badge list")
    fake_badges.list = mocker.Mock(return_value=[])
    fp.badges = fake_badges
    fp.members_all.list = mocker.Mock(return_value=[])
    fp.users.list.return_value = []
    fp.empty_repo = False
    return fp


@pytest.fixture
def fake_gitlab(mocker, fake_group, fake_project):
    fgl = mocker.Mock(name="Fake Gitlab")
    fgl.url = "https://fake.gitlab.url/"
    fgl.projects = mocker.Mock()
    fgl.projects.get = mocker.Mock(return_value=fake_project)
    fgl.groups = mocker.Mock()
    fgl.groups.get = mocker.Mock(return_value=fake_group)
    return fgl
