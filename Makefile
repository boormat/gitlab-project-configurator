.PHONY: clean clean-dist clean-build build docs help

# ==================================================================================================
# Variables
# ==================================================================================================

MODULES:=gpc
PACKAGE_NAME=gpc
EXECUTABLE_NAME:=gpc

PYTHON:=$(shell which python3)
POETRY:=$(PYTHON) -m poetry
PIP:=$(PYTHON) -m pip
DOCKER_BUILD:=docker build

MIN_COVERAGE_PERCENT:=84
SHELL:=/bin/bash
GPC_COLOR=force
POETRY_VERSION=$(shell cat .poetry-version | grep -v '\#')
PIP_VERSION=$(shell cat .pip-version | grep -v '\#')

# ==================================================================================================
# General Functions
# ==================================================================================================

# pip install --user for developer environment, without for CI
ifeq ($(CI),true)
	PIP_INSTALL_OPT=
else
	PIP_INSTALL_OPT=--user
endif

define BROWSER_PYSCRIPT
import os, webbrowser, sys
from urllib.request import pathname2url
webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT
BROWSER := python3 -c "$$BROWSER_PYSCRIPT"

define PRINT_HELP_PYSCRIPT
import re, sys
for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-30s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

CHANGELOG_HISTORY_START_VERSION=3.0.0


# ==================================================================================================
# help target
# ==================================================================================================

help:  ## This help message
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)


# ==================================================================================================
# do-it-all targets
# ==================================================================================================

all: dev style checks dists test  ## Build everything

dev: clean-venv ensure-poetry mk-venv poetry-install  ## Setup dev environment

initial-dev: update style  ## Initial dev environment

authors:
	poetry run git fame -e

# ==================================================================================================
# Install targets
# ==================================================================================================

ensure-poetry:
	$(PYTHON) -m pip install $(PIP_INSTALL_OPT) --upgrade \
		"poetry==$(POETRY_VERSION)" \
		"pip==$(PIP_VERSION)"
	@echo "ensure your local python install is in your PATH"

poetry-install: mk-venv
	$(POETRY) install

mk-venv: clean-venv
	# use that to configure a symbolic link to the virtualenv in .venv
	mkdir -p .venv

.venv: mk-venv

clean-venv:
	@rm -fr .venv

poetry-install-inspect: poetry-install
	$(POETRY) show -t

# ==================================================================================================
# Code formatting targets
# ==================================================================================================

style: isort black pyupgrade  ## Format code

isort:
	$(POETRY) run isort $(MODULES)

black:
	$(POETRY) run black $(MODULES)

pyupgrade:
	@( $(POETRY) run pyupgrade --py36-plus \
		$(shell find $(MODULES) \
			-name "*.py" \
			-type f \
			-not -path 'nestor_cli/vendors/*' \
		) \
	) || true

format: style


# ==================================================================================================
# Static checks targets
# ==================================================================================================

checks: isort-check black-check flake8 pylint mypy bandit pydocstyle  ## Static analysis

isort-check:
	$(POETRY) run isort -c $(MODULES)

black-check:
	$(POETRY) run black --check $(MODULES)

flake8:
	$(POETRY) run flake8 $(PACKAGE_NAME)

pylint:
	$(POETRY) run pylint --rcfile=.pylintrc.toml --output-format=colorized $(MODULES)

mypy:
	# Static type checker only enabled on methods that uses Python Type Annotations
	$(POETRY) run mypy --config-file .mypy.ini $(MODULES)

bandit:
	$(POETRY) run bandit -c .bandit.yml -r $(MODULES)

pydocstyle:
	$(POETRY) run pydocstyle $(MODULES)

gitlint:
	$(POETRY) run gitlint

checkout-mr:
	# expect a single MR won't hold more than 50 commits
	git fetch --update-shallow --depth=50 || true
	git fetch origin master
	git checkout -B "$(CI_BUILD_REF_NAME)" "$(CI_BUILD_REF)"  # sync git branch name with CI branch name
	git merge-base origin/master HEAD

gitlint-mr: checkout-mr
	$(POETRY) run gitlint --commits "`git merge-base origin/master HEAD`..HEAD"

clean-mypy:
	rm -rf .mypy_cache || true

sc: style check

sct: style check test

sctb: style check test bdd-test

sctu: style check test upload

sctur: style check test upload-ready

# ==================================================================================================
# Test targets
# ==================================================================================================

DUMMY_JIRA_PASSWORD?=dummy_password

bdd-test:  ## Execute behave test with junit report, /!\ With junit, log and print are not displayed
	DUMMY_JIRA_PASSWORD=$(DUMMY_JIRA_PASSWORD) \
		$(POETRY) run behave behave_test/integration_tests/features --junit \
		--junit-directory behave_reports  --format rerun --outfile rerun_failing.features || \
		(test -f rerun_failing.features && $(POETRY) run behave @rerun_failing.features)

bdd-test-debug: ## Execute Behave test without junit report to display print, exception and log
	DUMMY_JIRA_PASSWORD=$(DUMMY_JIRA_PASSWORD) \
		$(POETRY) run behave behave_test/integration_tests/features \
			--no-capture --no-capture-stderr --no-logcapture --no-color \
			--logging-level=DEBUG --logging-format="%(levelname)-7s [%(name)-30s] %(message)s" $(BEHAVE_ARGS)

test:  ## Execute unit tests
	$(POETRY) run pytest -vv \
		-m "not integration_tests" \
		--log-level=DEBUG \
		--html=report-unit-tests.html --self-contained-html \
		--junitxml=report-unit-tests.xml -o junit_suite_name=unit_tests \
		$(MODULES)

test-v:  ## Execute verbose unit tests
	$(POETRY) run pytest -vv \
		-m "not integration_tests" \
		--log-level=DEBUG \
		--html=report-unit-tests.html --self-contained-html \
		--junitxml=report-unit-tests.xml -o junit_suite_name=unit_tests \
		$(MODULES)

test-coverage:
	$(POETRY) run py.test -v \
		-m "not integration_tests" \
		--log-level=DEBUG \
		--cov $(PACKAGE_NAME) \
		--cov-report html:coverage_html \
		--cov-report term \
		--cov-report xml:report-coverage.xml \
		--cov-fail-under=$(MIN_COVERAGE_PERCENT) \
		--junitxml=report-unit-tests.xml -o junit_suite_name=unit_tests \
		$(MODULES)

test-integration:
	$(POETRY) run py.test -v \
		-m "integration_tests" \
		--log-level=DEBUG \
		--junitxml=report-integration-tests.xml -o junit_suite_name=integration_tests \
		$(MODULES)

watch-unittests:  ## Watch unit test (restrict with: make watch-unittests TEST=myunittesttoway)
	$(POETRY) run ptw $(TEST) \
		--onfail "notify-send -t 500 -i face-angry \"Unit tests failed!\"" \
		--onpass "notify-send -t 500 -i face-wink \"Unit tests succeed!\"" \
		-- -vv -m "not integration_tests"


# ==================================================================================================
# Distribution packages targets
# ==================================================================================================
############################## TODO

export-prod-requirements:
	mkdir -p dist
	$(POETRY) export --format requirements.txt -o dist/prod-requirements.txt --without-hashes

dists: export-prod-requirements  ## Build all distribution packages
	$(POETRY) build

build: dists

clean-dist:
	rm -rfv build dist/


# ==================================================================================================
# Docker targets
# ==================================================================================================

docker: clean-dist build docker-build  ## Build docker image

docker-build:
	$(DOCKER_BUILD) .


# ==================================================================================================
# Misc targets
# ==================================================================================================

shell:
	$(POETRY) shell

ctags:
	find -name '*.py' -exec ctags -a {} \;

update: poetry-update  ## Update dependencies

poetry-update:
	$(POETRY) update

update-recreate: update style check test

githook: style

compile-schema:
	mkdir -p dist/
	$(POETRY) run python scripts/compile-schema.py


# ==================================================================================================
# Publish targets
# ==================================================================================================

rebase-master:
	git pull --rebase origin master

amend-push:
	git commit --amend --all --no-edit -s
	git push origin --force

push: githook
	git push origin --all
	git push origin --tags

publish: clean-dist dists
	$(POETRY) publish -r nexus || true

upload:
	nestor upload -am

upload-ready:
	nestor upload -ram -w @swlabs/nestor/reviewers

# ==================================================================================================
# Clean targets
# ==================================================================================================

clean: clean-dist clean-docs clean-mypy clean-venv  ## Clean environment
	find . -name '__pycache__'  -exec rm -rf {} \; || true
	find . -name '.cache'  -exec rm -rf {} \; || true
	find . -name '*.egg-info'  -exec rm -rf {} \; || true
	find . -name "*.pyc" -exec rm -f {} \; || true
	rm -rf .pytest_cache || true
	rm -rf coverage_html || true

# ==================================================================================================
# Documentation targets
# ==================================================================================================

DOCS_EXCLUSION=$(foreach m, $(MODULES), $m/tests)

docs: clean-docs docs-generate-apidoc docs-generate-changelog docs-run-sphinx  ## Build online documentation

docs-generate-apidoc:
	$(POETRY) run sphinx-apidoc \
		--force \
		--separate \
		--module-first \
		--no-toc \
		--doc-project "API Reference" \
		-o docs/source/reference \
		$(PACKAGE_NAME) \
			$(DOCS_EXCLUSION)

docs-generate-changelog:
	$(POETRY) run cz changelog --incremental --start-rev $(CHANGELOG_HISTORY_START_VERSION)


docs-run-sphinx:
	$(POETRY) run make -C docs/ html

clean-docs:
	rm -rf docs/_build docs/source/reference/gpc*.rst

docs-open:
	xdg-open docs/_build/html/index.html


# ==================================================================================================
# Run targets
# ==================================================================================================

run:
	GPC_DEFAULTS_FILE=gpc-default.cfg \
	GPC_TRACE_FILENAME=debug.log \
		$(POETRY) run $(EXECUTABLE_NAME) --help

# ==================================================================================================
# Aliases to gracefully handle typos on poor dev's terminal
# ==================================================================================================

check: checks
coverage: test-coverage
devel: dev
develop: dev
doc: docs
dist: dists
styles: style
test-unit: test
tests: test
unittest: test
unittests: test
unit-tests: test
ut: test
it: test-integration
wheels: wheel
